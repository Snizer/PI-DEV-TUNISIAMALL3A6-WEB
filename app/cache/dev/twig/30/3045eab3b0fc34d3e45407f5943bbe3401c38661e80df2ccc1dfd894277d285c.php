<?php

/* LabessBackofficeBundle::layout.html.twig */
class __TwigTemplate_3ee6036d8cabed3083ba33761a1e5698bead9e691a436527b9535ba95540c5fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
\t<!-- Meta, title, CSS, favicons, etc. -->
\t<meta charset=\"utf-8\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

\t<title>Home</title>

\t<!-- Bootstrap core CSS -->

\t<link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

\t<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/fonts/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

\t<!-- Custom styling plus plugins -->
\t<link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/css/custom.css"), "html", null, true);
        echo "\"  rel=\"stylesheet\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/css/maps/jquery-jvectormap-2.0.3.css"), "html", null, true);
        echo "\" />
\t<link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/css/icheck/flat/green.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
\t<link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/css/floatexamples.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />

\t<script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/nprogress.js"), "html", null, true);
        echo "\"></script>

\t<!--[if lt IE 9]>
\t<script src=\"../assets/js/ie8-responsive-file-warning.js\"></script>
\t<![endif]-->

\t<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
\t<!--[if lt IE 9]>
\t<script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
\t<script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
\t<![endif]-->

</head>


<body class=\"nav-md\">

\t<div class=\"container body\">
\t\t<div class=\"main_container\">
                    
                   
                    
\t\t\t<div class=\"col-md-3 left_col\">
\t\t\t\t<div class=\"left_col scroll-view\">

\t\t\t\t\t<div class=\"navbar nav_title\" style=\"border: 0;\">
\t\t\t\t\t\t<a href=\"";
        // line 53
        echo $this->env->getExtension('routing')->getPath("labess_backoffice_homepage");
        echo "\" class=\"site_title\"><i class=\"fa fa-paw\"></i> <span>TunisiaMall</span></a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"clearfix\"></div>

\t\t\t\t\t<!-- menu prile quick info -->
\t\t\t\t\t<div class=\"profile\">
\t\t\t\t\t\t<div class=\"profile_pic\">
\t\t\t\t\t\t\t<img src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/images/dali.jpg"), "html", null, true);
        echo "\" alt=\"...\" class=\"img-circle profile_img\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"profile_info\">
\t\t\t\t\t\t\t<span>Welcome,</span>
\t\t\t\t\t\t\t<h2>Dali</h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- /menu prile quick info -->

\t\t\t\t\t<br />

\t\t\t\t\t<!-- sidebar menu -->
\t\t\t\t\t<div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">

\t\t\t\t\t\t<div class=\"menu_section\">
\t\t\t\t\t\t\t<h3>General</h3>
                                                        <br/>
                                                        <br/>
                                                        <br/>
\t\t\t\t\t\t\t<ul class=\"nav side-menu\">
\t\t\t\t\t\t\t\t<li><a><i class=\"fa fa-edit\"></i> Gestion Des Marques <span class=\"fa fa-chevron-down\"></span></a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav child_menu\" style=\"display: none\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("labess_backoffice_ajoutermarque");
        echo "\">Ajouter Marque</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 84
        echo $this->env->getExtension('routing')->getPath("labess_backoffice_affichemarque");
        echo "\">Liste Des Marques</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
                                                                <li><a><i class=\"fa fa-edit\"></i> Gestion Des Responsables <span class=\"fa fa-chevron-down\"></span></a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav child_menu\" style=\"display: none\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"\">Ajouter Responsable</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 92
        echo $this->env->getExtension('routing')->getPath("labess_backoffice_afficheresponsable");
        echo "\">Liste Des Responsables</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
                                                                <li><a><i class=\"fa fa-edit\"></i> Gestion Des Utilisateurs <span class=\"fa fa-chevron-down\"></span></a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav child_menu\" style=\"display: none\">
                                  \t                                        <li><a href=\"";
        // line 98
        echo $this->env->getExtension('routing')->getPath("labess_backoffice_afficheclient");
        echo "\">Liste Des Utilisateurs</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
                                                                <li><a><i class=\"fa fa-edit\"></i> Gestion Des Locaux <span class=\"fa fa-chevron-down\"></span></a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav child_menu\" style=\"display: none\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"empty.html\">Menu2.1</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"empty.html\">Meny2.2s</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- /sidebar menu -->

\t\t\t\t\t<!-- /menu footer buttons -->
\t\t\t\t\t<div class=\"sidebar-footer hidden-small\">
\t\t\t\t\t\t<a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">
\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t</a>
\t\t\t\t\t\t<a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">
\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t</a>
\t\t\t\t\t\t<a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">
\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t</a>
\t\t\t\t\t\t<a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\">
\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- /menu footer buttons -->
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<!-- top navigation -->
\t\t\t<div class=\"top_nav\">

\t\t\t\t<div class=\"nav_menu\">
\t\t\t\t\t<nav class=\"\" role=\"navigation\">
\t\t\t\t\t\t<div class=\"nav toggle\">
\t\t\t\t\t\t\t<a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</nav>
\t\t\t\t</div>

\t\t\t</div>
\t\t\t<!-- /top navigation -->


\t\t\t<!-- page content -->
\t\t\t<div class=\"right_col\" role=\"main\">

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12 col-sm-12 col-xs-12\">
\t\t\t\t\t\t<div class=\"dashboard_graph\">

\t\t\t\t\t\t\t<div class=\"row x_title\">
\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t<h3>Home <small>Page</small></h3>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-md-6\">

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"col-md-12 col-sm-12 col-xs-12\">
\t\t\t\t\t\t\t\t...
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<br />

\t\t\t\t<!-- footer content -->

\t\t\t\t<footer>
\t\t\t\t\t<div class=\"copyright-info\">
\t\t\t\t\t\t<p class=\"pull-right\">Developped By Labess Team &copy; 2016\t\t
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t</footer>
\t\t\t\t<!-- /footer content -->
\t\t\t</div>
\t\t\t<!-- /page content -->

\t\t</div>

\t</div>

\t<div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
\t\t<ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
\t\t</ul>
\t\t<div class=\"clearfix\"></div>
\t\t<div id=\"notif-group\" class=\"tabbed_notifications\"></div>
\t</div>

\t<script src=\"";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

\t<!-- gauge js -->
\t<script type=\"text/javascript\" src=\"";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/gauge/gauge.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 204
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/gauge/gauge_demo.js"), "html", null, true);
        echo "\"></script>
\t<!-- chart js -->
\t<script src=\"";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/chartjs/chart.min.js"), "html", null, true);
        echo "\"></script>
\t<!-- bootstrap progress js -->
\t<script src=\"";
        // line 208
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/progressbar/bootstrap-progressbar.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/nicescroll/jquery.nicescroll.min.js"), "html", null, true);
        echo "\"></script>
\t<!-- icheck -->
\t<script src=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/icheck/icheck.min.js"), "html", null, true);
        echo "\"></script>
\t<!-- daterangepicker -->
\t<script type=\"text/javascript\" src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/moment/moment.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/datepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>

\t<script src=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/custom.js"), "html", null, true);
        echo "\"></script>

\t<!-- flot js -->
\t<!--[if lte IE 8]><script type=\"text/javascript\" src=\"js/excanvas.min.js\"></script><![endif]-->
\t<script type=\"text/javascript\" src=\"";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/flot/jquery.flot.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/flot/jquery.flot.pie.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/flot/jquery.flot.orderBars.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/flot/jquery.flot.time.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/flot/date.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 225
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/flot/jquery.flot.spline.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/flot/jquery.flot.stack.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 227
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/flot/curvedLines.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 228
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/flot/jquery.flot.resize.js"), "html", null, true);
        echo "\"></script>
\t<script>
\t\t\$(document).ready(function() {
\t\t\t// [17, 74, 6, 39, 20, 85, 7]
\t\t\t//[82, 23, 66, 9, 99, 6, 2]
\t\t\tvar data1 = [
\t\t\t\t[gd(2012, 1, 1), 17],
\t\t\t\t[gd(2012, 1, 2), 74],
\t\t\t\t[gd(2012, 1, 3), 6],
\t\t\t\t[gd(2012, 1, 4), 39],
\t\t\t\t[gd(2012, 1, 5), 20],
\t\t\t\t[gd(2012, 1, 6), 85],
\t\t\t\t[gd(2012, 1, 7), 7]
\t\t\t];

\t\t\tvar data2 = [
\t\t\t\t[gd(2012, 1, 1), 82],
\t\t\t\t[gd(2012, 1, 2), 23],
\t\t\t\t[gd(2012, 1, 3), 66],
\t\t\t\t[gd(2012, 1, 4), 9],
\t\t\t\t[gd(2012, 1, 5), 119],
\t\t\t\t[gd(2012, 1, 6), 6],
\t\t\t\t[gd(2012, 1, 7), 9]
\t\t\t];
\t\t\t\$(\"#canvas_dahs\").length && \$.plot(\$(\"#canvas_dahs\"), [
\t\t\t\tdata1, data2
\t\t\t], {
\t\t\t\tseries: {
\t\t\t\t\tlines: {
\t\t\t\t\t\tshow: false,
\t\t\t\t\t\tfill: true
\t\t\t\t\t},
\t\t\t\t\tsplines: {
\t\t\t\t\t\tshow: true,
\t\t\t\t\t\ttension: 0.4,
\t\t\t\t\t\tlineWidth: 1,
\t\t\t\t\t\tfill: 0.4
\t\t\t\t\t},
\t\t\t\t\tpoints: {
\t\t\t\t\t\tradius: 0,
\t\t\t\t\t\tshow: true
\t\t\t\t\t},
\t\t\t\t\tshadowSize: 2
\t\t\t\t},
\t\t\t\tgrid: {
\t\t\t\t\tverticalLines: true,
\t\t\t\t\thoverable: true,
\t\t\t\t\tclickable: true,
\t\t\t\t\ttickColor: \"#d5d5d5\",
\t\t\t\t\tborderWidth: 1,
\t\t\t\t\tcolor: '#fff'
\t\t\t\t},
\t\t\t\tcolors: [\"rgba(38, 185, 154, 0.38)\", \"rgba(3, 88, 106, 0.38)\"],
\t\t\t\txaxis: {
\t\t\t\t\ttickColor: \"rgba(51, 51, 51, 0.06)\",
\t\t\t\t\tmode: \"time\",
\t\t\t\t\ttickSize: [1, \"day\"],
\t\t\t\t\t//tickLength: 10,
\t\t\t\t\taxisLabel: \"Date\",
\t\t\t\t\taxisLabelUseCanvas: true,
\t\t\t\t\taxisLabelFontSizePixels: 12,
\t\t\t\t\taxisLabelFontFamily: 'Verdana, Arial',
\t\t\t\t\taxisLabelPadding: 10
\t\t\t\t\t\t//mode: \"time\", timeformat: \"%m/%d/%y\", minTickSize: [1, \"day\"]
\t\t\t\t},
\t\t\t\tyaxis: {
\t\t\t\t\tticks: 8,
\t\t\t\t\ttickColor: \"rgba(51, 51, 51, 0.06)\",
\t\t\t\t},
\t\t\t\ttooltip: false
\t\t\t});

\t\t\tfunction gd(year, month, day) {
\t\t\t\treturn new Date(year, month - 1, day).getTime();
\t\t\t}
\t\t});
\t</script>

\t<!-- worldmap -->
\t<script type=\"text/javascript\" src=\"";
        // line 307
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/maps/jquery-jvectormap-2.0.3.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 308
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/maps/gdp-data.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 309
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/maps/jquery-jvectormap-world-mill-en.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 310
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/maps/jquery-jvectormap-us-aea-en.js"), "html", null, true);
        echo "\"></script>
\t<!-- pace -->
\t<script src=\"";
        // line 312
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/pace/pace.min.js"), "html", null, true);
        echo "\"></script>
\t<script>
\t\t\$(function() {
\t\t\t\$('#world-map-gdp').vectorMap({
\t\t\t\tmap: 'world_mill_en',
\t\t\t\tbackgroundColor: 'transparent',
\t\t\t\tzoomOnScroll: false,
\t\t\t\tseries: {
\t\t\t\t\tregions: [{
\t\t\t\t\t\tvalues: gdpData,
\t\t\t\t\t\tscale: ['#E6F2F0', '#149B7E'],
\t\t\t\t\t\tnormalizeFunction: 'polynomial'
\t\t\t\t\t}]
\t\t\t\t},
\t\t\t\tonRegionTipShow: function(e, el, code) {
\t\t\t\t\tel.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
\t\t\t\t}
\t\t\t});
\t\t});
\t</script>
\t<!-- skycons -->
\t<script src=\"";
        // line 333
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("backEnd/js/skycons/skycons.min.js"), "html", null, true);
        echo "\"></script>
\t<script>
\t\tvar icons = new Skycons({
\t\t\t\t\"color\": \"#73879C\"
\t\t\t}),
\t\t\tlist = [
\t\t\t\t\"clear-day\", \"clear-night\", \"partly-cloudy-day\",
\t\t\t\t\"partly-cloudy-night\", \"cloudy\", \"rain\", \"sleet\", \"snow\", \"wind\",
\t\t\t\t\"fog\"
\t\t\t],
\t\t\ti;

\t\tfor (i = list.length; i--;)
\t\t\ticons.set(list[i], list[i]);

\t\ticons.play();
\t</script>

\t<!-- dashbord linegraph -->
\t<script>
\t\tvar doughnutData = [{
\t\t\tvalue: 30,
\t\t\tcolor: \"#455C73\"
\t\t}, {
\t\t\tvalue: 30,
\t\t\tcolor: \"#9B59B6\"
\t\t}, {
\t\t\tvalue: 60,
\t\t\tcolor: \"#BDC3C7\"
\t\t}, {
\t\t\tvalue: 100,
\t\t\tcolor: \"#26B99A\"
\t\t}, {
\t\t\tvalue: 120,
\t\t\tcolor: \"#3498DB\"
\t\t}];
\t\tvar myDoughnut = new Chart(document.getElementById(\"canvas1\").getContext(\"2d\")).Doughnut(doughnutData);
\t</script>
\t<!-- /dashbord linegraph -->
\t<!-- datepicker -->
\t<script type=\"text/javascript\">
\t\t\$(document).ready(function() {

\t\t\tvar cb = function(start, end, label) {
\t\t\t\tconsole.log(start.toISOString(), end.toISOString(), label);
\t\t\t\t\$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
\t\t\t\t//alert(\"Callback has fired: [\" + start.format('MMMM D, YYYY') + \" to \" + end.format('MMMM D, YYYY') + \", label = \" + label + \"]\");
\t\t\t}

\t\t\tvar optionSet1 = {
\t\t\t\tstartDate: moment().subtract(29, 'days'),
\t\t\t\tendDate: moment(),
\t\t\t\tminDate: '01/01/2012',
\t\t\t\tmaxDate: '12/31/2015',
\t\t\t\tdateLimit: {
\t\t\t\t\tdays: 60
\t\t\t\t},
\t\t\t\tshowDropdowns: true,
\t\t\t\tshowWeekNumbers: true,
\t\t\t\ttimePicker: false,
\t\t\t\ttimePickerIncrement: 1,
\t\t\t\ttimePicker12Hour: true,
\t\t\t\tranges: {
\t\t\t\t\t'Today': [moment(), moment()],
\t\t\t\t\t'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
\t\t\t\t\t'Last 7 Days': [moment().subtract(6, 'days'), moment()],
\t\t\t\t\t'Last 30 Days': [moment().subtract(29, 'days'), moment()],
\t\t\t\t\t'This Month': [moment().startOf('month'), moment().endOf('month')],
\t\t\t\t\t'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
\t\t\t\t},
\t\t\t\topens: 'left',
\t\t\t\tbuttonClasses: ['btn btn-default'],
\t\t\t\tapplyClass: 'btn-small btn-primary',
\t\t\t\tcancelClass: 'btn-small',
\t\t\t\tformat: 'MM/DD/YYYY',
\t\t\t\tseparator: ' to ',
\t\t\t\tlocale: {
\t\t\t\t\tapplyLabel: 'Submit',
\t\t\t\t\tcancelLabel: 'Clear',
\t\t\t\t\tfromLabel: 'From',
\t\t\t\t\ttoLabel: 'To',
\t\t\t\t\tcustomRangeLabel: 'Custom',
\t\t\t\t\tdaysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
\t\t\t\t\tmonthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
\t\t\t\t\tfirstDay: 1
\t\t\t\t}
\t\t\t};
\t\t\t\$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
\t\t\t\$('#reportrange').daterangepicker(optionSet1, cb);
\t\t\t\$('#reportrange').on('show.daterangepicker', function() {
\t\t\t\tconsole.log(\"show event fired\");
\t\t\t});
\t\t\t\$('#reportrange').on('hide.daterangepicker', function() {
\t\t\t\tconsole.log(\"hide event fired\");
\t\t\t});
\t\t\t\$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
\t\t\t\tconsole.log(\"apply event fired, start/end dates are \" + picker.startDate.format('MMMM D, YYYY') + \" to \" + picker.endDate.format('MMMM D, YYYY'));
\t\t\t});
\t\t\t\$('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
\t\t\t\tconsole.log(\"cancel event fired\");
\t\t\t});
\t\t\t\$('#options1').click(function() {
\t\t\t\t\$('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
\t\t\t});
\t\t\t\$('#options2').click(function() {
\t\t\t\t\$('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
\t\t\t});
\t\t\t\$('#destroy').click(function() {
\t\t\t\t\$('#reportrange').data('daterangepicker').remove();
\t\t\t});
\t\t});
\t</script>
\t<script>
\t\tNProgress.done();
\t</script>
\t<!-- /datepicker -->
\t<!-- /footer content -->
</body>

</html>
";
    }

    public function getTemplateName()
    {
        return "LabessBackofficeBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  470 => 333,  446 => 312,  441 => 310,  437 => 309,  433 => 308,  429 => 307,  347 => 228,  343 => 227,  339 => 226,  335 => 225,  331 => 224,  327 => 223,  323 => 222,  319 => 221,  315 => 220,  308 => 216,  303 => 214,  299 => 213,  294 => 211,  289 => 209,  285 => 208,  280 => 206,  275 => 204,  271 => 203,  265 => 200,  160 => 98,  151 => 92,  140 => 84,  135 => 82,  110 => 60,  100 => 53,  71 => 27,  67 => 26,  62 => 24,  58 => 23,  54 => 22,  50 => 21,  44 => 18,  40 => 17,  35 => 15,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* */
/* <head>*/
/* 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">*/
/* 	<!-- Meta, title, CSS, favicons, etc. -->*/
/* 	<meta charset="utf-8">*/
/* 	<meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/* 	<meta name="viewport" content="width=device-width, initial-scale=1">*/
/* */
/* 	<title>Home</title>*/
/* */
/* 	<!-- Bootstrap core CSS -->*/
/* */
/* 	<link href="{{ asset('backEnd/css/bootstrap.min.css') }}" rel="stylesheet">*/
/* */
/* 	<link href="{{ asset('backEnd/fonts/css/font-awesome.min.css') }}" rel="stylesheet">*/
/* 	<link href="{{ asset('backEnd/css/animate.min.css') }}" rel="stylesheet">*/
/* */
/* 	<!-- Custom styling plus plugins -->*/
/* 	<link href="{{ asset('backEnd/css/custom.css') }}"  rel="stylesheet">*/
/* 	<link rel="stylesheet" type="text/css" href="{{ asset('backEnd/css/maps/jquery-jvectormap-2.0.3.css') }}" />*/
/* 	<link href="{{ asset('backEnd/css/icheck/flat/green.css') }}" rel="stylesheet" />*/
/* 	<link href="{{ asset('backEnd/css/floatexamples.css') }}" rel="stylesheet" type="text/css" />*/
/* */
/* 	<script src="{{ asset('backEnd/js/jquery.min.js') }}"></script>*/
/* 	<script src="{{ asset('backEnd/js/nprogress.js') }}"></script>*/
/* */
/* 	<!--[if lt IE 9]>*/
/* 	<script src="../assets/js/ie8-responsive-file-warning.js"></script>*/
/* 	<![endif]-->*/
/* */
/* 	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->*/
/* 	<!--[if lt IE 9]>*/
/* 	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>*/
/* 	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>*/
/* 	<![endif]-->*/
/* */
/* </head>*/
/* */
/* */
/* <body class="nav-md">*/
/* */
/* 	<div class="container body">*/
/* 		<div class="main_container">*/
/*                     */
/*                    */
/*                     */
/* 			<div class="col-md-3 left_col">*/
/* 				<div class="left_col scroll-view">*/
/* */
/* 					<div class="navbar nav_title" style="border: 0;">*/
/* 						<a href="{{ path('labess_backoffice_homepage') }}" class="site_title"><i class="fa fa-paw"></i> <span>TunisiaMall</span></a>*/
/* 					</div>*/
/* 					<div class="clearfix"></div>*/
/* */
/* 					<!-- menu prile quick info -->*/
/* 					<div class="profile">*/
/* 						<div class="profile_pic">*/
/* 							<img src="{{ asset('backEnd/images/dali.jpg') }}" alt="..." class="img-circle profile_img">*/
/* 						</div>*/
/* 						<div class="profile_info">*/
/* 							<span>Welcome,</span>*/
/* 							<h2>Dali</h2>*/
/* 						</div>*/
/* 					</div>*/
/* 					<!-- /menu prile quick info -->*/
/* */
/* 					<br />*/
/* */
/* 					<!-- sidebar menu -->*/
/* 					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">*/
/* */
/* 						<div class="menu_section">*/
/* 							<h3>General</h3>*/
/*                                                         <br/>*/
/*                                                         <br/>*/
/*                                                         <br/>*/
/* 							<ul class="nav side-menu">*/
/* 								<li><a><i class="fa fa-edit"></i> Gestion Des Marques <span class="fa fa-chevron-down"></span></a>*/
/* 									<ul class="nav child_menu" style="display: none">*/
/* 										<li><a href="{{ path('labess_backoffice_ajoutermarque') }}">Ajouter Marque</a>*/
/* 										</li>*/
/* 										<li><a href="{{ path('labess_backoffice_affichemarque') }}">Liste Des Marques</a>*/
/* 										</li>*/
/* 									</ul>*/
/* 								</li>*/
/*                                                                 <li><a><i class="fa fa-edit"></i> Gestion Des Responsables <span class="fa fa-chevron-down"></span></a>*/
/* 									<ul class="nav child_menu" style="display: none">*/
/* 										<li><a href="">Ajouter Responsable</a>*/
/* 										</li>*/
/* 										<li><a href="{{ path('labess_backoffice_afficheresponsable') }}">Liste Des Responsables</a>*/
/* 										</li>*/
/* 									</ul>*/
/* 								</li>*/
/*                                                                 <li><a><i class="fa fa-edit"></i> Gestion Des Utilisateurs <span class="fa fa-chevron-down"></span></a>*/
/* 									<ul class="nav child_menu" style="display: none">*/
/*                                   	                                        <li><a href="{{ path('labess_backoffice_afficheclient') }}">Liste Des Utilisateurs</a>*/
/* 										</li>*/
/* 									</ul>*/
/* 								</li>*/
/*                                                                 <li><a><i class="fa fa-edit"></i> Gestion Des Locaux <span class="fa fa-chevron-down"></span></a>*/
/* 									<ul class="nav child_menu" style="display: none">*/
/* 										<li><a href="empty.html">Menu2.1</a>*/
/* 										</li>*/
/* 										<li><a href="empty.html">Meny2.2s</a>*/
/* 										</li>*/
/* 									</ul>*/
/* 								</li>*/
/* 							</ul>*/
/* 						</div>*/
/* 					</div>*/
/* 					<!-- /sidebar menu -->*/
/* */
/* 					<!-- /menu footer buttons -->*/
/* 					<div class="sidebar-footer hidden-small">*/
/* 						<a data-toggle="tooltip" data-placement="top" title="Settings">*/
/* 							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>*/
/* 						</a>*/
/* 						<a data-toggle="tooltip" data-placement="top" title="FullScreen">*/
/* 							<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>*/
/* 						</a>*/
/* 						<a data-toggle="tooltip" data-placement="top" title="Lock">*/
/* 							<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>*/
/* 						</a>*/
/* 						<a data-toggle="tooltip" data-placement="top" title="Logout">*/
/* 							<span class="glyphicon glyphicon-off" aria-hidden="true"></span>*/
/* 						</a>*/
/* 					</div>*/
/* 					<!-- /menu footer buttons -->*/
/* 				</div>*/
/* 			</div>*/
/* */
/* 			<!-- top navigation -->*/
/* 			<div class="top_nav">*/
/* */
/* 				<div class="nav_menu">*/
/* 					<nav class="" role="navigation">*/
/* 						<div class="nav toggle">*/
/* 							<a id="menu_toggle"><i class="fa fa-bars"></i></a>*/
/* 						</div>*/
/* 					</nav>*/
/* 				</div>*/
/* */
/* 			</div>*/
/* 			<!-- /top navigation -->*/
/* */
/* */
/* 			<!-- page content -->*/
/* 			<div class="right_col" role="main">*/
/* */
/* 				<div class="row">*/
/* 					<div class="col-md-12 col-sm-12 col-xs-12">*/
/* 						<div class="dashboard_graph">*/
/* */
/* 							<div class="row x_title">*/
/* 								<div class="col-md-6">*/
/* 									<h3>Home <small>Page</small></h3>*/
/* 								</div>*/
/* 								<div class="col-md-6">*/
/* */
/* 								</div>*/
/* 							</div>*/
/* */
/* 							<div class="col-md-12 col-sm-12 col-xs-12">*/
/* 								...*/
/* 							</div>*/
/* */
/* 							<div class="clearfix"></div>*/
/* 						</div>*/
/* 					</div>*/
/* */
/* 				</div>*/
/* 				<br />*/
/* */
/* 				<!-- footer content -->*/
/* */
/* 				<footer>*/
/* 					<div class="copyright-info">*/
/* 						<p class="pull-right">Developped By Labess Team &copy; 2016		*/
/* 						</p>*/
/* 					</div>*/
/* 					<div class="clearfix"></div>*/
/* 				</footer>*/
/* 				<!-- /footer content -->*/
/* 			</div>*/
/* 			<!-- /page content -->*/
/* */
/* 		</div>*/
/* */
/* 	</div>*/
/* */
/* 	<div id="custom_notifications" class="custom-notifications dsp_none">*/
/* 		<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/* 		</ul>*/
/* 		<div class="clearfix"></div>*/
/* 		<div id="notif-group" class="tabbed_notifications"></div>*/
/* 	</div>*/
/* */
/* 	<script src="{{ asset('backEnd/js/bootstrap.min.js') }}"></script>*/
/* */
/* 	<!-- gauge js -->*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/gauge/gauge.min.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/gauge/gauge_demo.js') }}"></script>*/
/* 	<!-- chart js -->*/
/* 	<script src="{{ asset('backEnd/js/chartjs/chart.min.js') }}"></script>*/
/* 	<!-- bootstrap progress js -->*/
/* 	<script src="{{ asset('backEnd/js/progressbar/bootstrap-progressbar.min.js') }}"></script>*/
/* 	<script src="{{ asset('backEnd/js/nicescroll/jquery.nicescroll.min.js') }}"></script>*/
/* 	<!-- icheck -->*/
/* 	<script src="{{ asset('backEnd/js/icheck/icheck.min.js') }}"></script>*/
/* 	<!-- daterangepicker -->*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/moment/moment.min.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/datepicker/daterangepicker.js') }}"></script>*/
/* */
/* 	<script src="{{ asset('backEnd/js/custom.js') }}"></script>*/
/* */
/* 	<!-- flot js -->*/
/* 	<!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/flot/jquery.flot.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/flot/jquery.flot.pie.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/flot/jquery.flot.orderBars.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/flot/jquery.flot.time.min.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/flot/date.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/flot/jquery.flot.spline.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/flot/jquery.flot.stack.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/flot/curvedLines.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/flot/jquery.flot.resize.js') }}"></script>*/
/* 	<script>*/
/* 		$(document).ready(function() {*/
/* 			// [17, 74, 6, 39, 20, 85, 7]*/
/* 			//[82, 23, 66, 9, 99, 6, 2]*/
/* 			var data1 = [*/
/* 				[gd(2012, 1, 1), 17],*/
/* 				[gd(2012, 1, 2), 74],*/
/* 				[gd(2012, 1, 3), 6],*/
/* 				[gd(2012, 1, 4), 39],*/
/* 				[gd(2012, 1, 5), 20],*/
/* 				[gd(2012, 1, 6), 85],*/
/* 				[gd(2012, 1, 7), 7]*/
/* 			];*/
/* */
/* 			var data2 = [*/
/* 				[gd(2012, 1, 1), 82],*/
/* 				[gd(2012, 1, 2), 23],*/
/* 				[gd(2012, 1, 3), 66],*/
/* 				[gd(2012, 1, 4), 9],*/
/* 				[gd(2012, 1, 5), 119],*/
/* 				[gd(2012, 1, 6), 6],*/
/* 				[gd(2012, 1, 7), 9]*/
/* 			];*/
/* 			$("#canvas_dahs").length && $.plot($("#canvas_dahs"), [*/
/* 				data1, data2*/
/* 			], {*/
/* 				series: {*/
/* 					lines: {*/
/* 						show: false,*/
/* 						fill: true*/
/* 					},*/
/* 					splines: {*/
/* 						show: true,*/
/* 						tension: 0.4,*/
/* 						lineWidth: 1,*/
/* 						fill: 0.4*/
/* 					},*/
/* 					points: {*/
/* 						radius: 0,*/
/* 						show: true*/
/* 					},*/
/* 					shadowSize: 2*/
/* 				},*/
/* 				grid: {*/
/* 					verticalLines: true,*/
/* 					hoverable: true,*/
/* 					clickable: true,*/
/* 					tickColor: "#d5d5d5",*/
/* 					borderWidth: 1,*/
/* 					color: '#fff'*/
/* 				},*/
/* 				colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],*/
/* 				xaxis: {*/
/* 					tickColor: "rgba(51, 51, 51, 0.06)",*/
/* 					mode: "time",*/
/* 					tickSize: [1, "day"],*/
/* 					//tickLength: 10,*/
/* 					axisLabel: "Date",*/
/* 					axisLabelUseCanvas: true,*/
/* 					axisLabelFontSizePixels: 12,*/
/* 					axisLabelFontFamily: 'Verdana, Arial',*/
/* 					axisLabelPadding: 10*/
/* 						//mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]*/
/* 				},*/
/* 				yaxis: {*/
/* 					ticks: 8,*/
/* 					tickColor: "rgba(51, 51, 51, 0.06)",*/
/* 				},*/
/* 				tooltip: false*/
/* 			});*/
/* */
/* 			function gd(year, month, day) {*/
/* 				return new Date(year, month - 1, day).getTime();*/
/* 			}*/
/* 		});*/
/* 	</script>*/
/* */
/* 	<!-- worldmap -->*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/maps/jquery-jvectormap-2.0.3.min.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/maps/gdp-data.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/maps/jquery-jvectormap-world-mill-en.js') }}"></script>*/
/* 	<script type="text/javascript" src="{{ asset('backEnd/js/maps/jquery-jvectormap-us-aea-en.js') }}"></script>*/
/* 	<!-- pace -->*/
/* 	<script src="{{ asset('backEnd/js/pace/pace.min.js') }}"></script>*/
/* 	<script>*/
/* 		$(function() {*/
/* 			$('#world-map-gdp').vectorMap({*/
/* 				map: 'world_mill_en',*/
/* 				backgroundColor: 'transparent',*/
/* 				zoomOnScroll: false,*/
/* 				series: {*/
/* 					regions: [{*/
/* 						values: gdpData,*/
/* 						scale: ['#E6F2F0', '#149B7E'],*/
/* 						normalizeFunction: 'polynomial'*/
/* 					}]*/
/* 				},*/
/* 				onRegionTipShow: function(e, el, code) {*/
/* 					el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');*/
/* 				}*/
/* 			});*/
/* 		});*/
/* 	</script>*/
/* 	<!-- skycons -->*/
/* 	<script src="{{ asset('backEnd/js/skycons/skycons.min.js') }}"></script>*/
/* 	<script>*/
/* 		var icons = new Skycons({*/
/* 				"color": "#73879C"*/
/* 			}),*/
/* 			list = [*/
/* 				"clear-day", "clear-night", "partly-cloudy-day",*/
/* 				"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",*/
/* 				"fog"*/
/* 			],*/
/* 			i;*/
/* */
/* 		for (i = list.length; i--;)*/
/* 			icons.set(list[i], list[i]);*/
/* */
/* 		icons.play();*/
/* 	</script>*/
/* */
/* 	<!-- dashbord linegraph -->*/
/* 	<script>*/
/* 		var doughnutData = [{*/
/* 			value: 30,*/
/* 			color: "#455C73"*/
/* 		}, {*/
/* 			value: 30,*/
/* 			color: "#9B59B6"*/
/* 		}, {*/
/* 			value: 60,*/
/* 			color: "#BDC3C7"*/
/* 		}, {*/
/* 			value: 100,*/
/* 			color: "#26B99A"*/
/* 		}, {*/
/* 			value: 120,*/
/* 			color: "#3498DB"*/
/* 		}];*/
/* 		var myDoughnut = new Chart(document.getElementById("canvas1").getContext("2d")).Doughnut(doughnutData);*/
/* 	</script>*/
/* 	<!-- /dashbord linegraph -->*/
/* 	<!-- datepicker -->*/
/* 	<script type="text/javascript">*/
/* 		$(document).ready(function() {*/
/* */
/* 			var cb = function(start, end, label) {*/
/* 				console.log(start.toISOString(), end.toISOString(), label);*/
/* 				$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));*/
/* 				//alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");*/
/* 			}*/
/* */
/* 			var optionSet1 = {*/
/* 				startDate: moment().subtract(29, 'days'),*/
/* 				endDate: moment(),*/
/* 				minDate: '01/01/2012',*/
/* 				maxDate: '12/31/2015',*/
/* 				dateLimit: {*/
/* 					days: 60*/
/* 				},*/
/* 				showDropdowns: true,*/
/* 				showWeekNumbers: true,*/
/* 				timePicker: false,*/
/* 				timePickerIncrement: 1,*/
/* 				timePicker12Hour: true,*/
/* 				ranges: {*/
/* 					'Today': [moment(), moment()],*/
/* 					'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],*/
/* 					'Last 7 Days': [moment().subtract(6, 'days'), moment()],*/
/* 					'Last 30 Days': [moment().subtract(29, 'days'), moment()],*/
/* 					'This Month': [moment().startOf('month'), moment().endOf('month')],*/
/* 					'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]*/
/* 				},*/
/* 				opens: 'left',*/
/* 				buttonClasses: ['btn btn-default'],*/
/* 				applyClass: 'btn-small btn-primary',*/
/* 				cancelClass: 'btn-small',*/
/* 				format: 'MM/DD/YYYY',*/
/* 				separator: ' to ',*/
/* 				locale: {*/
/* 					applyLabel: 'Submit',*/
/* 					cancelLabel: 'Clear',*/
/* 					fromLabel: 'From',*/
/* 					toLabel: 'To',*/
/* 					customRangeLabel: 'Custom',*/
/* 					daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],*/
/* 					monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],*/
/* 					firstDay: 1*/
/* 				}*/
/* 			};*/
/* 			$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));*/
/* 			$('#reportrange').daterangepicker(optionSet1, cb);*/
/* 			$('#reportrange').on('show.daterangepicker', function() {*/
/* 				console.log("show event fired");*/
/* 			});*/
/* 			$('#reportrange').on('hide.daterangepicker', function() {*/
/* 				console.log("hide event fired");*/
/* 			});*/
/* 			$('#reportrange').on('apply.daterangepicker', function(ev, picker) {*/
/* 				console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));*/
/* 			});*/
/* 			$('#reportrange').on('cancel.daterangepicker', function(ev, picker) {*/
/* 				console.log("cancel event fired");*/
/* 			});*/
/* 			$('#options1').click(function() {*/
/* 				$('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);*/
/* 			});*/
/* 			$('#options2').click(function() {*/
/* 				$('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);*/
/* 			});*/
/* 			$('#destroy').click(function() {*/
/* 				$('#reportrange').data('daterangepicker').remove();*/
/* 			});*/
/* 		});*/
/* 	</script>*/
/* 	<script>*/
/* 		NProgress.done();*/
/* 	</script>*/
/* 	<!-- /datepicker -->*/
/* 	<!-- /footer content -->*/
/* </body>*/
/* */
/* </html>*/
/* */
