<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/panier')) {
            // panierafficher
            if (rtrim($pathinfo, '/') === '/panier') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'panierafficher');
                }

                return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\panierController::afficherAction',  '_route' => 'panierafficher',);
            }

            if (0 === strpos($pathinfo, '/panier/checkout')) {
                // checkout1
                if ($pathinfo === '/panier/checkout1') {
                    return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\panierController::checkout1Action',  '_route' => 'checkout1',);
                }

                // checkout2
                if ($pathinfo === '/panier/checkout2') {
                    return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\panierController::checkout2Action',  '_route' => 'checkout2',);
                }

                // checkout3
                if ($pathinfo === '/panier/checkout3') {
                    return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\panierController::checkout3Action',  '_route' => 'checkout3',);
                }

                // checkout4
                if ($pathinfo === '/panier/checkout4') {
                    return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\panierController::checkout4Action',  '_route' => 'checkout4',);
                }

            }

            // listecommandes
            if ($pathinfo === '/panier/listecommandes') {
                return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\panierController::listecommandesAction',  '_route' => 'listecommandes',);
            }

            // commande
            if ($pathinfo === '/panier/commande') {
                return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\panierController::commandeAction',  '_route' => 'commande',);
            }

            if (0 === strpos($pathinfo, '/panier/wishlist')) {
                // wishlist
                if ($pathinfo === '/panier/wishlist') {
                    return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\panierController::wishlistAction',  '_route' => 'wishlist',);
                }

                // utilisateurprofil
                if ($pathinfo === '/panier/wishlist') {
                    return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\panierController::utilisateurprofilAction',  '_route' => 'utilisateurprofil',);
                }

            }

        }

        // home
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'home');
            }

            return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\HomeController::indexAction',  '_route' => 'home',);
        }

        // register
        if ($pathinfo === '/register') {
            return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\HomeController::registerAction',  '_route' => 'register',);
        }

        // contact
        if ($pathinfo === '/contact') {
            return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\HomeController::contactAction',  '_route' => 'contact',);
        }

        if (0 === strpos($pathinfo, '/produits')) {
            // produits
            if (rtrim($pathinfo, '/') === '/produits') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'produits');
                }

                return array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\produitController::indexAction',  '_route' => 'produits',);
            }

            // presentation
            if (preg_match('#^/produits/(?P<nom>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'presentation')), array (  '_controller' => 'Tunisiamall\\tunisiamallBundle\\Controller\\produitController::presentationAction',));
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
