<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/AppBundle/DataFixtures/ORM/LoadUserData.php

namespace utilisateurs\utilisateursBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use utilisateurs\utilisateursBundle\Entity\User;

class UserData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface {

    private $container;

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {
        $user1 = new User();
        $user1->setCin('121221');
        $user1->setAdresse('12 Avenue Ibn Sina 123');
        $user1->setSexe('Femme');
        $user1->setPrenom('Saida');
        $user1->setNom('Khouja');
        $user1->setUsername('Saida');
        $user1->setEmail('saidan@gmail.com');
        $user1->setEnabled(1);
        $user1->setRole('CLIENT');
        $user1->setEtat('1');
        $user1->setPassword($this->container->get('security.encoder_factory')->getEncoder($user1)->encodePassword('saida', $user1->getSalt()));
        $manager->persist($user1);


        $user2 = new User();
        $user2->setCin('2223223');
        $user2->setAdresse('12 Avenue Taher sfar');
        $user2->setSexe('Homme');
        $user2->setPrenom('Taipon');
        $user2->setNom('Jaf');
        $user2->setUsername('Taipon');
        $user2->setEmail('Tibou@gmail.com');
        $user2->setEnabled(1);
        $user2->setRole('CLIENT');
        $user2->setEtat('1');
        $user2->setPassword($this->container->get('security.encoder_factory')->getEncoder($user2)->encodePassword('111', $user2->getSalt()));
        $manager->persist($user2);


        $user3 = new User();
        $user3->setCin('22288983');
        $user3->setAdresse('12 Avenue Hbib bourguiba');
        $user3->setSexe('Homme');
        $user3->setPrenom('Stoufaaa');
        $user3->setNom('Jaf');
        $user3->setUsername('stoufa');
        $user3->setEmail('stoufa@gmail.com');
        $user3->setEnabled(1);
        $user3->setRole('CLIENT');
        $user3->setEtat('1');
        $user3->setPassword($this->container->get('security.encoder_factory')->getEncoder($user3)->encodePassword('131', $user3->getSalt()));
        $manager->persist($user3);



        $manager->flush();

        $this->addReference('user1', $user1);
        $this->addReference('user2', $user2);
        $this->addReference('user3', $user3);
    }

    public function getOrder() {

        return 1;
    }

}
