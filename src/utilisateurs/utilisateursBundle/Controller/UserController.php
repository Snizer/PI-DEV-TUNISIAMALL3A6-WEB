<?php

namespace utilisateurs\utilisateursBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Tunisiamall\tunisiamallBundle\Services\GetFacture;
use  Symfony\Component\HttpFoundation\Response;
class UserController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
   
     public function facturesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $factures = $em->getRepository('TunisiamalltunisiamallBundle:Commandes')->byFacture($this->getUser());
        
        return $this->render('utilisateursutilisateursBundle:Default:layout/facture.html.twig', array('factures' => $factures));
    }
     public function facturesPDFAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $facture = $em->getRepository('TunisiamalltunisiamallBundle:Commandes')->findOneBy(array('utilisateur' => $this->getUser(),
                                                                                     'valider' => 1,
                                                                                     'id' => $id));
        
        if (!$facture) {
            $this->get('session')->getFlashBag()->add('error', 'Error has been occured');
            return $this->redirect($this->generateUrl('factures'));
        }
        
//        $this->container->get('setNewFacture')->facture($facture)->Output('Facture.pdf');
////        
////      
////        
////        
////        
//       $response = new Response();
//        $response->headers->set('Content-type' , 'application/pdf');
////        
//       return $response;
////        
////        
        
//        $html = $this->renderView('utilisateursutilisateursBundle:Default:layout/facturePDF.html.twig', array('facture' => $facture));
//
//return new Response(
//    $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
//    200,
//    array(
//        'Content-Type'          => 'application/pdf',
//        'Content-Disposition'   => 'attachment; filename="file.pdf"'
//    )
//);
//           
//        
          return $this->render('utilisateursutilisateursBundle:Default:layout/facturePDF.html.twig', array('facture' => $facture));
    }
    
}
