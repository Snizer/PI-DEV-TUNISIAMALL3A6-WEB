<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/AppBundle/DataFixtures/ORM/LoadUserData.php

namespace Tunisiamall\tunisiamallBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Tunisiamall\tunisiamallBundle\Entity\Marque;
use Tunisiamall\tunisiamallBundle\Entity\Produit;
use Tunisiamall\tunisiamallBundle\DataFixtures\ORM\MarqueData;

class ProduitData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        $produit1 = new Produit();
        $produit1->setIdMarque($this->getReference('marque1'));
        $produit1->setDescription('Jegging en jean femme basic, brut, 2 fausses poches avant, 2 poches dos, fermeture zippée boutonnée.');
        $produit1->setNom('jegging en jean brut');
        $produit1->setNombrePoint('2');
        $produit1->setPhoto('img/4.jpg');
        $produit1->setPrix('298');
        $produit1->setQuantite('212');
        $produit1->setQuantiteVendu('20');
        $produit1->setReference('REF323');
        $produit1->setTaille('m');
        $produit1->setTauxReduction('2');
        $produit1->setType('Femme jeans');
        $manager->persist($produit1);

         $produit2 = new Produit();
        $produit2->setIdMarque($this->getReference('marque1'));
        $produit2->setDescription('LOREM LOREM LOREM UPSON Djkds jjskkzjizldjezio');
        $produit2->setNom('Baskets');
        $produit2->setNombrePoint('2');
        $produit2->setPhoto('img/2.jpg');
        $produit2->setPrix('211');
        $produit2->setQuantite('23');
        $produit2->setQuantiteVendu('20');
        $produit2->setReference('REF6533');
        $produit2->setTaille('s');
        $produit2->setTauxReduction('2');
        $produit2->setType('Femme chaussures baskets'); 
        $manager->persist($produit2);
        
        
         $produit3= new Produit();
        $produit3->setIdMarque($this->getReference('marque1'));
        $produit3->setDescription('Blouson noir fermeture a pression Blouson noir fermeture a pression');
        $produit3->setNom('Blouson noir fermeture a pression');
        $produit3->setNombrePoint('2');
        $produit3->setPhoto('img/1.jpg');
        $produit3->setPrix('211');
        $produit3->setQuantite('23');
        $produit3->setQuantiteVendu('20');
        $produit3->setReference('REF3253');
        $produit3->setTaille('s');
        $produit3->setTauxReduction('2');
        $produit3->setType('Femme vestes'); 
        $manager->persist($produit3);
        
        $produit4= new Produit();
        $produit4->setIdMarque($this->getReference('marque2'));
        $produit4->setDescription('Sweat femme, gris chiné, imprimé boudeuse, loose, col rond, manches 3/4.matière : polyester 33 %,');
        $produit4->setNom('sweat imprimé boudeuse gris chiné');
        $produit4->setNombrePoint('2');
        $produit4->setPhoto('img/3.jpg');
        $produit4->setPrix('211');
        $produit4->setQuantite('23');
        $produit4->setQuantiteVendu('20');
        $produit4->setReference('REF321222');
        $produit4->setTaille('s');
        $produit4->setTauxReduction('2');
        $produit4->setType('Femme sweat-shirts'); 
        $manager->persist($produit4);

        
         $produit5= new Produit();
        $produit5->setIdMarque($this->getReference('marque2'));
        $produit5->setDescription('foulard imprimé cachemire écru et bleu marine');
        $produit5->setNom('foulard imprimé cachemire écru et bleu marine');
        $produit5->setNombrePoint('2');
        $produit5->setPhoto('img/5.jpg');
        $produit5->setPrix('211');
        $produit5->setQuantite('23');
        $produit5->setQuantiteVendu('20');
        $produit5->setReference('REF321091');
        $produit5->setTaille('s');
        $produit5->setTauxReduction('2');
        $produit5->setType('Femme accessoires'); 
        $manager->persist($produit5);
        
        
         $produit6= new Produit();
        $produit6->setIdMarque($this->getReference('marque2'));
        $produit6->setDescription('Bottines à talons femme, camel, suédine, franges œillets doré, fermeture zip, talons 7 cm.matière ');
        $produit6->setNom('bottines à franges camel');
        $produit6->setNombrePoint('2');
        $produit6->setPhoto('img/6.jpg');
        $produit6->setPrix('211');
        $produit6->setQuantite('23');
        $produit6->setQuantiteVendu('20');
        $produit6->setReference('REF3287');
        $produit6->setTaille('s');
        $produit6->setTauxReduction('2');
        $produit6->setType('Femme chaussures bottines'); 
        $manager->persist($produit6);
        
        
        $produit7= new Produit();
        $produit7->setIdMarque($this->getReference('marque2'));
        $produit7->setDescription('Bottines à talons femme, camel, suédine, franges œillets doré, fermeture zip, talons 7 cm.matière ');
        $produit7->setNom('escarpins suédine noirs');
        $produit7->setNombrePoint('2');
        $produit7->setPhoto('img/9.jpg');
        $produit7->setPrix('211');
        $produit7->setQuantite('23');
        $produit7->setQuantiteVendu('20');
        $produit7->setReference('REF31');
        $produit7->setTaille('s');
        $produit7->setTauxReduction('2');
        $produit7->setType('Femme chaussures escarpins'); 
        $manager->persist($produit7);
        
        
        $manager->flush();

        $this->addReference('produit1', $produit1);
        $this->addReference('produit2', $produit2);
        $this->addReference('produit3', $produit3);
        $this->addReference('produit4', $produit4);
        $this->addReference('produit5', $produit5);
        $this->addReference('produit6', $produit6);
        $this->addReference('produit7', $produit7);
        //faire la relation avec les prochaines fixtures qu'on va realiser 
        //pour chaque fixture on cree une reference
    }

    public function getOrder() {

        return 3;
    }

}
