<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/AppBundle/DataFixtures/ORM/LoadUserData.php

namespace Tunisiamall\tunisiamallBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Tunisiamall\tunisiamallBundle\Entity\Marque;
use utilisateurs\utilisateursBundle\Entity\User;
use utilisateurs\utilisateursBundle\DataFixtures\ORM\UserData;

class MarqueData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        $marque1 = new Marque();
        $marque1->setIdResponsable($this->getReference('user1'));
        $marque1->setNom('Zara');
        $marque1->setSeuilachat('120');
        $manager->persist($marque1);


        $marque2 = new Marque();
        $marque2->setIdResponsable($this->getReference('user2'));
        $marque2->setNom('Jennyfer');
        $marque2->setSeuilachat('120');
        $manager->persist($marque2);



        $marque3 = new Marque();
        $marque3->setIdResponsable($this->getReference('user3'));
        $marque3->setNom('Bershka');
        $marque3->setSeuilachat('150');
        $manager->persist($marque3);


        $manager->flush();

        $this->addReference('marque1', $marque1);
        $this->addReference('marque2', $marque2);
        $this->addReference('marque3', $marque3);

        //faire la relation avec les prochaines fixtures qu'on va realiser 
        //pour chaque fixture on cree une reference
    }

    public function getOrder() {

        return 2;
    }

}
