<?php

namespace Tunisiamall\tunisiamallBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction()
    {
        
         $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->findBy(array(), array('id' => 'desc'));
        
        return $this->render('TunisiamalltunisiamallBundle:Default:Home/accueil.html.twig', array('produits' => $produits));
    }
    
    public function contactAction()
    {
        return $this->render('TunisiamalltunisiamallBundle:Default:Home/contact.html.twig');
    }
    public function produitsroomAction()
    {
        
         $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->findAll();
        
        return $this->render('TunisiamalltunisiamallBundle:Default:Home/marques3d.html.twig', array('produits' => $produits));
    }
    
}
