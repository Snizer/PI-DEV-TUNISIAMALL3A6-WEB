<?php

namespace Tunisiamall\tunisiamallBundle\Controller;
use Tunisiamall\tunisiamallBundle\Form\RechercheType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class produitController extends Controller {

    public function indexAction() {
        
        
        $session = $this->getRequest()->getSession();
         $em = $this->getDoctrine()->getManager();
        $findproduits = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->findAll(); 
        
        if ($session->has('panier'))
            $panier = $session->get('panier');
        else
            $panier = false;
        //commence par page1 et limite 3 /page
      $produits = $this->get('knp_paginator')->paginate($findproduits,$this->get('request')->query->get('page', 1),6);
    
   

      
      
        return $this->render('TunisiamalltunisiamallBundle:Default:produit/produits.html.twig', array('produits' => $produits,'panier' => $panier));
       
    }

    public function presentationAction($id) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $produit = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->find($id);
         if ($session->has('panier'))
            $panier = $session->get('panier');
        else
            $panier = false;
               $utilisateur = $this->container->get('security.context')->getToken()->getUser();
        return $this->render('TunisiamalltunisiamallBundle:Default:produit/presentation.html.twig', array('produit' => $produit,'panier' => $panier,'utilisateur'=>$utilisateur));
    }

    public function menuHAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Homme';
        $findcategories = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategorie($categ);
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
    }
       public function menuHcAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Homme';
        $categex = 'Homme accessoires';
         $categex1 = 'Homme chaussures';
        $findcategories = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategoriec($categ,$categex,$categex1);
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
    }
    public function menuFcAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Femme';
        $categex = 'Femme accessoires';
         $categex1 = 'Femme chaussures';
        $findcategories = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategoriec($categ,$categex,$categex1);
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
    }
    public function menuEcAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Enfant';
        $categex = 'Enfant accessoires';
         $categex1 = 'Enfant chaussures';
        $findcategories = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategoriec($categ,$categex,$categex1);
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
    }
    
 public function menuHSAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Homme chaussures';
        $findcategories = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategorie($categ);
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
    }
    public function menuESAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Enfant chaussures';
   $findcategories = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategorie($categ);
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
        }
    public function menuEAAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Enfant accessoires';
   $findcategories = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategorie($categ);
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
        }
    
    public function menuFSAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Femme chaussures';
   $findcategories = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategorie($categ);
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
        }
         public function menuFAAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Femme accessoires';
   $findcategories = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategorie($categ);
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
        }
        public function menuHAAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Homme accessoires';
   $findcategories = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategorie($categ);
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
        }
    public function menuFAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Femme';
//        $findcategoriesprods = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategorie($categ);
//        $findcategories = $this->get('knp_paginator')->paginate($findcategoriesprods,$this->get('request')->query->get('page', 1),3);
//       
//        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
   $findcategories = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategorie($categ);
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
        }

    public function menuEAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Enfant';
//        $findcategoriesprods = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategorie($categ);
//        $findcategories = $this->get('knp_paginator')->paginate($findcategoriesprods,$this->get('request')->query->get('page', 1),3);
//       
//        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
    $findcategories = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->listeprodbycategorie($categ);
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listcateg.html.twig', array('findcategories' => $findcategories));
        
    }

    public function categorieAction($categorie) {
         $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $findproduits = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->findBy(array('type' => $categorie));
          if ($session->has('panier'))
            $panier = $session->get('panier');
        else
            $panier = false;
        
        $produits = $this->get('knp_paginator')->paginate($findproduits,$this->get('request')->query->get('page', 1),6);
       
        return $this->render('TunisiamalltunisiamallBundle:Default:produit/produits.html.twig', array('produits' => $produits,'panier' => $panier));
    }

    public function marqueAction($marque) {
         $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $findproduits = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->findBy(array('idMarque' => $marque));
        if ($session->has('panier'))
            $panier = $session->get('panier');
        else
            $panier = false;
        $produits = $this->get('knp_paginator')->paginate($findproduits,$this->get('request')->query->get('page', 1),6);
       
        return $this->render('TunisiamalltunisiamallBundle:Default:produit/produits.html.twig', array('produits' => $produits,'panier' => $panier));
    }
   
    
      public function rechercheAction(){
       
         $form= $this->createForm(new RechercheType());
        return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/recherche.html.twig',array('form'=>$form->createView()));   
         
     }
     public function rechercheTraitementAction($id=null)
     {
          $session = $this->getRequest()->getSession();
      $form = $this->createForm(new RechercheType());
        
        if ($this->get('request')->getMethod() == 'POST')
        {
            $form->bind($this->get('request'));
            $em = $this->getDoctrine()->getManager();
            $findproduits = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->recherche($form['recherche']->getData());
       if ($session->has('panier'))
            $panier = $session->get('panier');
        else
            $panier = false;
         } else {
            throw $this->createNotFoundException('Page not fouuuund 3:D.');
           // return $this->render('TunisiamalltunisiamallBundle:Default:Home/404.html.twig');
        }
         $produits = $this->get('knp_paginator')->paginate($findproduits,$this->get('request')->query->get('page', 1),6);
       
        return $this->render('TunisiamalltunisiamallBundle:Default:produit/produits.html.twig',array('produits'=>$produits,'panier' => $panier));  
     }
     public function countHAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Homme';
        $somme= $em->getRepository('TunisiamalltunisiamallBundle:Produit')->countCategorie($categ);
      return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/countH.html.twig', ['somme' => $somme]);
    }
    public function countFAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Femme';
        $somme= $em->getRepository('TunisiamalltunisiamallBundle:Produit')->countCategorie($categ);
      return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/countH.html.twig', ['somme' => $somme]);
    }
    public function countEAction() {
        $em = $this->getDoctrine()->getManager();
        $categ = 'Enfant';
        $somme= $em->getRepository('TunisiamalltunisiamallBundle:Produit')->countCategorie($categ);
      return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/countH.html.twig', ['somme' => $somme]);
    }
       
}
