<?php

namespace Tunisiamall\tunisiamallBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Tunisiamall\tunisiamallBundle\Repository\ProduitRepository;
use Tunisiamall\tunisiamallBundle\Form\AdressesType;
use Tunisiamall\tunisiamallBundle\Entity\Adresses;

use Tunisiamall\tunisiamallBundle\Entity\Panier;


class panierController extends Controller
{
      
        public function panierAction()
    {
   $session = $this->getRequest()->getSession();
        if (!$session->has('panier')) $session->set('panier', array());
//var_dump($session->get('panier'));
//die();
        //example : shows array(3) { [14]=> string(1) "1" [13]=> string(1) "9" [17]=> string(1) "1" }
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->findArray(array_keys($session->get('panier')));
       return $this->render('TunisiamalltunisiamallBundle:Default:panier/panier.html.twig', array('produits' => $produits,
                                                                                             'panier' => $session->get('panier')));
   
    }
    
 
    public function checkout2Action()
    {
        return $this->render('TunisiamalltunisiamallBundle:Default:panier/checkout2.html.twig');
    }
     public function checkout3Action()
    {
        return $this->render('TunisiamalltunisiamallBundle:Default:panier/checkout3.html.twig');
    }
     public function checkout4Action()
    {
        return $this->render('TunisiamalltunisiamallBundle:Default:panier/checkout4.html.twig');
    }
     public function listecommandesAction()
    {
        return $this->render('TunisiamalltunisiamallBundle:Default:panier/listecommandes.html.twig');
    }
     public function commandeAction()
    {
        return $this->render('TunisiamalltunisiamallBundle:Default:panier/commande.html.twig');
    }
     public function wishlistAction()
    {
         
          
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->findAll();
        
        
        return $this->render('TunisiamalltunisiamallBundle:Default:panier/wishlist.html.twig',array('produits' => $produits));
    }
    public function utilisateurprofilAction()
    {
        return $this->render('TunisiamalltunisiamallBundle:Default:panier/utilisateurprofil.html.twig');
    }
     public function supprimerAction($id)
    {
        $session = $this->getRequest()->getSession();
        $panier = $session->get('panier');
        
        if (array_key_exists($id, $panier))
        {
            unset($panier[$id]);
            $session->set('panier',$panier);
            $this->get('session')->getFlashBag()->add('success','Article deleted successfully.');
        }
        
          $utilisateur = $this->container->get('security.context')->getToken()->getUser();
        $query=$this->getDoctrine()->getManager()->createQuery('DELETE TunisiamalltunisiamallBundle:Panier m WHERE m.idClient = :idclient and m.idProduit= :idproduit');

$query->setParameter('idclient', $utilisateur->getId());
$query->setParameter('idproduit', $id);
$query->execute();

        return $this->redirect($this->generateUrl('panier')); 
    }
    
    public function ajouterAction($id)
    {
           $utilisateur = $this->container->get('security.context')->getToken()->getUser();
        
        $session = $this->getRequest()->getSession();
        
        if (!$session->has('panier')) $session->set('panier',array());
        $panier = $session->get('panier');
        
        if (array_key_exists($id, $panier)) {
            if ($this->getRequest()->query->get('qte') != null) $panier[$id] = $this->getRequest()->query->get('qte');
         $this->get('session')->getFlashBag()->add('success','Basket is successfully updated');
         
         
$query=$this->getDoctrine()->getManager()->createQuery('UPDATE TunisiamalltunisiamallBundle:Panier m SET  m.quantite = :quantite WHERE m.idClient = :idclient and m.idProduit= :idproduit');
$query->setParameter('quantite', $panier[$id]);
$query->setParameter('idclient', $utilisateur->getId());
$query->setParameter('idproduit', $id);
$query->execute();

         
         
        } else {
            if ($this->getRequest()->query->get('qte') != null)
                $panier[$id] = $this->getRequest()->query->get('qte');
            else
                $panier[$id] = 1;
          //add 
        $em = $this->getDoctrine()->getManager();
        $pa= new Panier();
     
        $pa->setIdClient($utilisateur);
        $pa->setQuantite($panier[$id]);
        
        $produit = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->find($id);
        $pa->setIdProduit($produit);
        
       
               
               
        $em->persist($pa);
        $em->flush();  
          $this->get('session')->getFlashBag()->add('success','Article added');
        }
            
        $session->set('panier',$panier);
        
        
        
        
        return $this->redirect($this->generateUrl('panier'));
        
        
       
    
                
        
        
    }
       public function menuAction()
    {
        $session = $this->getRequest()->getSession();
        if (!$session->has('panier'))
            $articles = 0;
        else
            $articles = count($session->get('panier'));
        
        return $this->render('TunisiamalltunisiamallBundle:Default:panier/countPanier.html.twig', array('articles' => $articles));
    }
    public function checkout1Action()
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.context')->getToken()->getUser();
        $entity = new Adresses();
        $form = $this->createForm(new AdressesType($em), $entity);
        
          $session = $this->getRequest()->getSession();
        if (!$session->has('panier')) $session->set('panier', array());
//var_dump($session->get('panier'));
//die();
        //example : shows array(3) { [14]=> string(1) "1" [13]=> string(1) "9" [17]=> string(1) "1" }
        
        $produits = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->findArray(array_keys($session->get('panier')));
        
        
        if ($this->get('request')->getMethod() == 'POST')
        {
            $form->handleRequest($this->getRequest());
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity->setIdclient($utilisateur);
               
                $em->persist($entity);
                $em->flush();
                
                return $this->redirect($this->generateUrl('checkout1'));
            }
        }
        
        return $this->render('TunisiamalltunisiamallBundle:Default:panier/checkout1.html.twig', array('utilisateur' => $utilisateur,
                                                                                                'form' => $form->createView(),'panier' => $session->get('panier'),'produits' => $produits));
     
    }
    
    
     public function adresseSuppressionAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('TunisiamalltunisiamallBundle:Adresses')->find($id);
        
        if ($this->container->get('security.context')->getToken()->getUser() != $entity->getIdclient() || !$entity)
            return $this->redirect ($this->generateUrl ('checkout1'));
        
        $em->remove($entity);
        $em->flush();
        
        return $this->redirect ($this->generateUrl ('checkout1'));
    }
    public function setLivraisonOnSession()
    {
        $session = $this->getRequest()->getSession();
        
        if (!$session->has('adresse')) $session->set('adresse',array());
        $adresse = $session->get('adresse');
        
        if ($this->getRequest()->request->get('livraison') != null && $this->getRequest()->request->get('facturation') != null)
        {
            $adresse['livraison'] = $this->getRequest()->request->get('livraison');
            $adresse['facturation'] = $this->getRequest()->request->get('facturation');
        } else {
            return $this->redirect($this->generateUrl('validation'));
        }
        
        $session->set('adresse',$adresse);
        return $this->redirect($this->generateUrl('checkout4'));
    }
    public function validationAction()
    {
        if ($this->get('request')->getMethod() == 'POST')
            $this->setLivraisonOnSession();
        
        $em = $this->getDoctrine()->getManager();
        $prepareCommande = $this->forward('TunisiamalltunisiamallBundle:commande:prepareCommande');
        $commande = $em->getRepository('TunisiamalltunisiamallBundle:Commandes')->find($prepareCommande->getContent());
        
        
    //   $session=$this->getRequest()->getSession();
//        $adresse=$session->get('adresse');
//      //  return $this->render('TunisiamalltunisiamallBundle:Default:panier/checkout4.html.twig', array('commande' => $commande));
   //  $prods = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->findArray(array_keys($session->get('panier')));
//     $livraison = $em->getRepository('TunisiamalltunisiamallBundle:Adresses')->find($adresse['livraison']);
//     $facturation = $em->getRepository('TunisiamalltunisiamallBundle:Adresses')->find($adresse['facturation']);
//    // var_dump($adresse);
//     
//          $utilisateur = $this->container->get('security.context')->getToken()->getUser();
//        return $this->render('TunisiamalltunisiamallBundle:Default:panier/checkout4.html.twig', array('produits' => $produits,'livraison' => $livraison,'facturation' => $facturation,'utilisateur' => $utilisateur,'panier' => $session->get('panier')));
//        
//        
        
       return $this->render('TunisiamalltunisiamallBundle:Default:panier/checkout4.html.twig', array('commande' => $commande)); 
    }
    
}
