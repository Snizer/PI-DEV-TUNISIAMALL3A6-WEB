<?php

namespace Tunisiamall\tunisiamallBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class marqueController extends Controller
{
    
    public function menuAction() {
        
        
        $em = $this->getDoctrine()->getManager();
        
       $marques = $em->getRepository('TunisiamalltunisiamallBundle:Marque')->findAll();
      return $this->render('TunisiamalltunisiamallBundle:Default:modulesUsed/listmarques.html.twig', array('marques' => $marques));
                     
        
        
    }
  
    public function categorieAction($categorie)
    {
         $em = $this->getDoctrine()->getManager();

       
       $produits = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->findBy(array('type' => $categorie));
        return $this->render('TunisiamalltunisiamallBundle:Default:produit/produits.html.twig',array('produits' => $produits));
    }
            
}
