<?php

namespace Tunisiamall\tunisiamallBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Tunisiamall\tunisiamallBundle\Repository\ProduitRepository;
use Tunisiamall\tunisiamallBundle\Form\AdressesType;
use Tunisiamall\tunisiamallBundle\Entity\Adresses;

use Tunisiamall\tunisiamallBundle\Entity\Commandes;
use Tunisiamall\tunisiamallBundle\Entity\Produit;
use Tunisiamall\tunisiamallBundle\Repository\CommandesRepository;

class commandeController extends Controller
{
       public function facture()
    {
     

        $em = $this->getDoctrine()->getManager();
        $generator = $this->container->get('security.secure_random');
        $session = $this->getRequest()->getSession();
        $adresse = $session->get('adresse');
        $panier = $session->get('panier');
        $commande = array();
        $totalHT = 0;
        $totalTVA = 0;
        
        $facturation = $em->getRepository('TunisiamalltunisiamallBundle:Adresses')->find($adresse['facturation']);
        $livraison = $em->getRepository('TunisiamalltunisiamallBundle:Adresses')->find($adresse['livraison']);
        $produits = $em->getRepository('TunisiamalltunisiamallBundle:Produit')->findArray(array_keys($session->get('panier')));
        
        foreach($produits as $produit)
        {
            $prixHT = ($produit->getPrix() * $panier[$produit->getId()]);
            $prixTTC = ((($prixHT*$produit->getTauxReduction())/100)  * $panier[$produit->getId()]);
            $totalHT += $prixHT;
            
            if (!isset($commande['reduction']['%'.$produit->getTauxReduction()]))
                $commande['reduction']['%'.$produit->getTauxReduction()] = round(( $prixHT -($prixHT-($prixHT*$produit->getTauxReduction())/100) ),2);
            else
                $commande['reduction']['%'.$produit->getTauxReduction()] += round(( $prixHT -($prixHT-($prixHT*$produit->getTauxReduction())/100) ),2);
            
            $totalTVA += round((($produit->getPrix()*$panier[$produit->getId()])-(($produit->getPrix()*$panier[$produit->getId()])-(($produit->getPrix()*$panier[$produit->getId()])*$produit->getTauxReduction()/100))),2);
                   
                    
             
            $commande['produit'][$produit->getId()] = array('nom' => $produit->getNom(),
                                                            'quantite' => $panier[$produit->getId()],
                                                            'prix' => round($produit->getPrix(),2),
                                                            'prixreduc' => round($produit->getPrix() - (($produit->getPrix()*$produit->getTauxReduction()/100)),2),
                                                            'tauxreduction' => $produit->getTauxReduction(),
                                                             'photo'=>$produit->getPhoto()
                                                             );
               $query=$this->getDoctrine()->getManager()->createQuery('UPDATE TunisiamalltunisiamallBundle:Produit m SET  m.quantiteVendu = m.quantiteVendu+:quantiteVendu WHERE m.id = :id');
   

$query->setParameter('id',$produit->getId());
    $query->setParameter('quantiteVendu', $panier[$produit->getId()]);
$query->execute();
        }  
        $utilisateur = $this->container->get('security.context')->getToken()->getUser();
        $commande['livraison'] = array('prenom' => $utilisateur->getPrenom(),
                                    'nom' => $utilisateur->getNom(),
                                   
                                    'adresse' => $livraison->getAdresse(),
                                    'zip' => $livraison->getZip(),
                                    'ville' => $livraison->getVille(),
                                    'pays' => $livraison->getPays()
                                   );

        $commande['facturation'] = array('prenom' =>$utilisateur->getPrenom(),
                                    'nom' =>  $utilisateur->getNom(),
                                    
                                    'adresse' => $facturation->getAdresse(),
                                    'zip' => $facturation->getZip(),
                                    'ville' => $facturation->getVille(),
                                    'pays' => $facturation->getPays()
                                    );

        $commande['prixtotal'] = round($totalHT,2);
        $commande['prixreduc'] = round($totalHT - $totalTVA,2);
        $commande['token'] = bin2hex($generator->nextBytes(20));
       
        return $commande;
    }
    
    public function prepareCommandeAction()
    {
        
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        if (!$session->has('commande'))
            $commande = new Commandes();
        else
            $commande = $em->getRepository('TunisiamalltunisiamallBundle:Commandes')->find($session->get('commande'));
        date_default_timezone_set('America/New_York');
        $commande->setDate(new \DateTime('now'));
        $commande->setUtilisateur($this->container->get('security.context')->getToken()->getUser());
        $commande->setValider(0);
        $commande->setReference(0);
        $commande->setCommande($this->facture());
        
        if (!$session->has('commande')) {
            $em->persist($commande);
            $session->set('commande',$commande);
        }
        
        $em->flush();
        
      
        return new Response($commande->getId());
    }
    
       
      public function validationCommandeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $commande = $em->getRepository('TunisiamalltunisiamallBundle:Commandes')->find($id);
        
        if (!$commande || $commande->getValider() == 1)
            throw $this->createNotFoundException('La commande n\'existe pas');
        
        $commande->setValider(1);
        $commande->setReference($this->container->get('setNewReference')->reference()); //Service
        $em->flush();   
           $session = $this->getRequest()->getSession();
         
        
        
        $session->remove('adresse');
        $session->remove('panier');
        $session->remove('commande');
        
        
        
        
        //Ici le mail de validation
        $message = \Swift_Message::newInstance()
                ->setSubject('Validation de votre commande')
                ->setFrom(array('fatma.jaafar404@gmail.com' => "Tunisia Mall"))
                ->setTo($commande->getUtilisateur()->getEmailCanonical())
                ->setCharset('utf-8')
                ->setContentType('text/html')
                ->setBody($this->renderView('TunisiamalltunisiamallBundle:Default:SwiftLayout/validationCommande.html.twig',array('utilisateur' => $commande->getUtilisateur(),'id'=>$id)));
        
        $this->get('mailer')->send($message);
        
        $this->get('session')->getFlashBag()->add('success','Votre commande est validé avec succès');
        return $this->redirect($this->generateUrl('factures'));
    }
}
