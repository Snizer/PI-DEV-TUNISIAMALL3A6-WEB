<?php

namespace Tunisiamall\tunisiamallBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdressesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pays','country',array('label'=>'Country', 'attr'=>array('class'=>'form-control')))
            ->add('ville','text',array( 'attr'=>array('class'=>'form-control')))
            ->add('adresse','textarea',array( 'attr'=>array('class'=>'form-control')))
            ->add('zip','number',array( 'attr'=>array('class'=>'form-control')))
            //->add('idclient')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tunisiamall\tunisiamallBundle\Entity\Adresses'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tunisiamall_tunisiamallbundle_adresses';
    }
}
