<?php
namespace Tunisiamall\tunisiamallBundle\Twig\Extension;

class reductionExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(new \Twig_SimpleFilter('reduction', array($this,'calculreduction')));
    }
    
    function calculreduction($prixHT,$reduction)
    {
         return round($prixHT-(($prixHT*$reduction)/100),2);
      
    }
    
    public function getName()
    {
        return 'reduction_extension';
    }
}