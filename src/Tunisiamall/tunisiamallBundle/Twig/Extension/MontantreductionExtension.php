<?php
namespace Tunisiamall\tunisiamallBundle\Twig\Extension;

class MontantreductionExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(new \Twig_SimpleFilter('montantreduc', array($this,'montantTva')));
    }
    
    function montantTva($prixHT,$reduc)
    {
        return round(( $prixHT -($prixHT-($prixHT*$reduc)/100) ),2);
    }
    
    public function getName()
    {
        return 'montant_tva_extension';
    }
}