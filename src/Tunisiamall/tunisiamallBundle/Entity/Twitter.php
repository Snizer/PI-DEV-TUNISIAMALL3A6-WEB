<?php

namespace Tunisiamall\tunisiamallBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Twitter
 *
 * @ORM\Table(name="twitter", uniqueConstraints={@ORM\UniqueConstraint(name="CONSUMER_KEY", columns={"CONSUMER_KEY", "CONSUMER_SECRET", "ACCESS_TOKEN", "ACCESS_TOKEN_SECRET"})}, indexes={@ORM\Index(name="FK_TWITTER_RESPONSABLE_ENSEIGNE", columns={"ID_RESPONSABLE"})})
 * @ORM\Entity
 */
class Twitter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="CONSUMER_KEY", type="string", length=200, nullable=false)
     */
    private $consumerKey;

    /**
     * @var string
     *
     * @ORM\Column(name="CONSUMER_SECRET", type="string", length=200, nullable=false)
     */
    private $consumerSecret;

    /**
     * @var string
     *
     * @ORM\Column(name="ACCESS_TOKEN", type="string", length=200, nullable=false)
     */
    private $accessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="ACCESS_TOKEN_SECRET", type="string", length=200, nullable=false)
     */
    private $accessTokenSecret;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="utilisateurs\utilisateursBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_RESPONSABLE", referencedColumnName="id")
     * })
     */
    private $idResponsable;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set consumerKey
     *
     * @param string $consumerKey
     *
     * @return Twitter
     */
    public function setConsumerKey($consumerKey)
    {
        $this->consumerKey = $consumerKey;

        return $this;
    }

    /**
     * Get consumerKey
     *
     * @return string
     */
    public function getConsumerKey()
    {
        return $this->consumerKey;
    }

    /**
     * Set consumerSecret
     *
     * @param string $consumerSecret
     *
     * @return Twitter
     */
    public function setConsumerSecret($consumerSecret)
    {
        $this->consumerSecret = $consumerSecret;

        return $this;
    }

    /**
     * Get consumerSecret
     *
     * @return string
     */
    public function getConsumerSecret()
    {
        return $this->consumerSecret;
    }

    /**
     * Set accessToken
     *
     * @param string $accessToken
     *
     * @return Twitter
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * Get accessToken
     *
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Set accessTokenSecret
     *
     * @param string $accessTokenSecret
     *
     * @return Twitter
     */
    public function setAccessTokenSecret($accessTokenSecret)
    {
        $this->accessTokenSecret = $accessTokenSecret;

        return $this;
    }

    /**
     * Get accessTokenSecret
     *
     * @return string
     */
    public function getAccessTokenSecret()
    {
        return $this->accessTokenSecret;
    }

    /**
     * Set idResponsable
     *
     * @param utilisateurs\utilisateursBundle\Entity\User $idResponsable
     *
     * @return Twitter
     */
    public function setIdResponsable(\utilisateurs\utilisateursBundle\Entity\User $idResponsable = null)
    {
        $this->idResponsable = $idResponsable;

        return $this;
    }

    /**
     * Get idResponsable
     *
     * @return \utilisateurs\utilisateursBundle\Entity\User
     */
    public function getIdResponsable()
    {
        return $this->idResponsable;
    }
}
