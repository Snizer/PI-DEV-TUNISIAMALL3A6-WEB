<?php

namespace Tunisiamall\tunisiamallBundle\Entity;
use utilisateurs\utilisateursBundle\Entity\Local;
use utilisateurs\utilisateursBundle\Entity\Marque;
use Doctrine\ORM\Mapping as ORM;

/**
 * Boutique
 *
 * @ORM\Table(name="boutique", indexes={@ORM\Index(name="FK_BOUTIQUE_MARQUE", columns={"ID_MARQUE"}), @ORM\Index(name="FK_BOUTIQUE_LOCAL", columns={"ID_LOCAL"})})
 * @ORM\Entity(repositoryClass="Tunisiamall\tunisiamallBundle\Repository\BoutiqueRepository")
 * @ORM\Entity
 */
class Boutique
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="LOGO", type="blob", length=65535, nullable=false)
     */
    private $logo;

    /**
     * @var \Local
     *
     * @ORM\ManyToOne(targetEntity="Local")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_LOCAL", referencedColumnName="ID")
     * })
     */
    private $idLocal;

    /**
     * @var \Marque
     *
     * @ORM\ManyToOne(targetEntity="Marque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_MARQUE", referencedColumnName="ID")
     * })
     */
    private $idMarque;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Boutique
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set idLocal
     *
     * @param \Tunisiamall\tunisiamallBundle\Entity\Local $idLocal
     *
     * @return Boutique
     */
    public function setIdLocal(\Tunisiamall\tunisiamallBundle\Entity\Local $idLocal = null)
    {
        $this->idLocal = $idLocal;

        return $this;
    }

    /**
     * Get idLocal
     *
     * @return \Tunisiamall\tunisiamallBundle\Entity\Local
     */
    public function getIdLocal()
    {
        return $this->idLocal;
    }

    /**
     * Set idMarque
     *
     * @param \Tunisiamall\tunisiamallBundle\Entity\Marque $idMarque
     *
     * @return Boutique
     */
    public function setIdMarque(\Tunisiamall\tunisiamallBundle\Entity\Marque $idMarque = null)
    {
        $this->idMarque = $idMarque;

        return $this;
    }

    /**
     * Get idMarque
     *
     * @return \Tunisiamall\tunisiamallBundle\Entity\Marque
     */
    public function getIdMarque()
    {
        return $this->idMarque;
    }
}
