<?php

namespace Tunisiamall\tunisiamallBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EspacePublicitaire
 *
 * @ORM\Table(name="espace_publicitaire", indexes={@ORM\Index(name="FK_ESPACE_PUBLICITAIRE_MARQUE", columns={"ID_MARQUE"})})
 * @ORM\Entity
 */
class EspacePublicitaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_DEBUT", type="date", nullable=false)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_FIN", type="date", nullable=false)
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="AFFICHE", type="string", length=50, nullable=false)
     */
    private $affiche;

    /**
     * @var \Marque
     *
     * @ORM\ManyToOne(targetEntity="Marque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_MARQUE", referencedColumnName="ID")
     * })
     */
    private $idMarque;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return EspacePublicitaire
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return EspacePublicitaire
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set affiche
     *
     * @param string $affiche
     *
     * @return EspacePublicitaire
     */
    public function setAffiche($affiche)
    {
        $this->affiche = $affiche;

        return $this;
    }

    /**
     * Get affiche
     *
     * @return string
     */
    public function getAffiche()
    {
        return $this->affiche;
    }

    /**
     * Set idMarque
     *
     * @param \Tunisiamall\tunisiamallBundle\Entity\Marque $idMarque
     *
     * @return EspacePublicitaire
     */
    public function setIdMarque(\Tunisiamall\tunisiamallBundle\Entity\Marque $idMarque = null)
    {
        $this->idMarque = $idMarque;

        return $this;
    }

    /**
     * Get idMarque
     *
     * @return \Tunisiamall\tunisiamallBundle\Entity\Marque
     */
    public function getIdMarque()
    {
        return $this->idMarque;
    }
}
