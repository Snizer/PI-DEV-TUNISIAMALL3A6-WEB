<?php

namespace Tunisiamall\tunisiamallBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarteFidilite
 *
 * @ORM\Table(name="carte_fidilite", indexes={@ORM\Index(name="FK_CARTE_FIDILITE_MARQUE", columns={"ID_MARQUE"}), @ORM\Index(name="FK_CARTE_FIDILITE_USER", columns={"ID_CLIENT"})})
 * @ORM\Entity
 */
class CarteFidilite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="NOMBRE_POINT", type="integer", nullable=false)
     */
    private $nombrePoint;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="\utilisateurs\utilisateursBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_CLIENT", referencedColumnName="id")
     * })
     */
    private $idClient;

    /**
     * @var \Marque
     *
     * @ORM\ManyToOne(targetEntity="Marque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_MARQUE", referencedColumnName="ID")
     * })
     */
    private $idMarque;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombrePoint
     *
     * @param integer $nombrePoint
     *
     * @return CarteFidilite
     */
    public function setNombrePoint($nombrePoint)
    {
        $this->nombrePoint = $nombrePoint;

        return $this;
    }

    /**
     * Get nombrePoint
     *
     * @return integer
     */
    public function getNombrePoint()
    {
        return $this->nombrePoint;
    }

    /**
     * Set idClient
     *
     * @param \utilisateurs\utilisateursBundle\Entity\User $idClient
     *
     * @return CarteFidilite
     */
    public function setIdClient(\utilisateurs\utilisateursBundle\Entity\User $idClient = null)
    {
        $this->idClient = $idClient;

        return $this;
    }

    /**
     * Get idClient
     *
     * @return \utilisateurs\utilisateursBundle\Entity\User
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * Set idMarque
     *
     * @param \Tunisiamall\tunisiamallBundle\Entity\Marque $idMarque
     *
     * @return CarteFidilite
     */
    public function setIdMarque(\Tunisiamall\tunisiamallBundle\Entity\Marque $idMarque = null)
    {
        $this->idMarque = $idMarque;

        return $this;
    }

    /**
     * Get idMarque
     *
     * @return \Tunisiamall\tunisiamallBundle\Entity\Marque
     */
    public function getIdMarque()
    {
        return $this->idMarque;
    }
}
