<?php

namespace Tunisiamall\tunisiamallBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Marque
 *
 * @ORM\Table(name="marque", indexes={@ORM\Index(name="Nom", columns={"Nom"}),@ORM\Index(name="FK_CARTE_FIDILITE_USER", columns={"ID_RESPONSABLE"}) })
 * @ORM\Entity
 */
class Marque
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var float
     *
     * @ORM\Column(name="seuilAchat", type="float", precision=10, scale=0, nullable=false)
     */
    private $seuilachat;

   /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="\utilisateurs\utilisateursBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_RESPONSABLE", referencedColumnName="id")
     * })
     */
    private $idResponsable;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Marque
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set seuilachat
     *
     * @param float $seuilachat
     *
     * @return Marque
     */
    public function setSeuilachat($seuilachat)
    {
        $this->seuilachat = $seuilachat;

        return $this;
    }

    /**
     * Get seuilachat
     *
     * @return float
     */
    public function getSeuilachat()
    {
        return $this->seuilachat;
    }

    /**
     * Set idResponsable
     *
     * @param @param \utilisateurs\utilisateursBundle\Entity\User $idResponsable
     *
     * @return Marque
     */
    public function setIdResponsable($idResponsable)
    {
        $this->idResponsable = $idResponsable;

        return $this;
    }

    /**
     * Get idResponsable
     *
     * @return \Tunisiamall\tunisiamallBundle\Entity\User
     */
    public function getIdResponsable()
    {
        return $this->idResponsable;
    }
}
