<?php

namespace Tunisiamall\tunisiamallBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Panier
 *
 * @ORM\Table(name="panier", indexes={@ORM\Index(name="FK_PANIER_CLIENT", columns={"ID_CLIENT"}), @ORM\Index(name="FK_PANIER_PRODUIT", columns={"ID_PRODUIT"})})
 * @ORM\Entity
 */
class Panier
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="QUANTITE", type="integer", nullable=false)
     */
    private $quantite;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="\utilisateurs\utilisateursBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_CLIENT", referencedColumnName="id")
     * })
     */
    private $idClient;

    /**
     * @var \Produit
     *
     * @ORM\ManyToOne(targetEntity="Produit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_PRODUIT", referencedColumnName="ID")
     * })
     */
    private $idProduit;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return Panier
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set idClient
     *
     * @param \utilisateurs\utilisateursBundle\Entity\User $idClient
     *
     * @return Panier
     */
    public function setIdClient(\utilisateurs\utilisateursBundle\Entity\User $idClient = null)
    {
        $this->idClient = $idClient;

        return $this;
    }

    /**
     * Get idClient
     *
     * @return \utilisateurs\utilisateursBundle\Entity\User
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * Set idProduit
     *
     * @param \Tunisiamall\tunisiamallBundle\Entity\Produit $idProduit
     *
     * @return Panier
     */
    public function setIdProduit(\Tunisiamall\tunisiamallBundle\Entity\Produit $idProduit = null)
    {
        $this->idProduit = $idProduit;

        return $this;
    }

    /**
     * Get idProduit
     *
     * @return \Tunisiamall\tunisiamallBundle\Entity\Produit
     */
    public function getIdProduit()
    {
        return $this->idProduit;
    }
}
