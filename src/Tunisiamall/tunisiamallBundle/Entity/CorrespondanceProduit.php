<?php

namespace Tunisiamall\tunisiamallBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CorrespondanceProduit
 *
 * @ORM\Table(name="correspondance_produit", uniqueConstraints={@ORM\UniqueConstraint(name="REFERENCE", columns={"REFERENCE"})}, indexes={@ORM\Index(name="FK_MARQUE", columns={"ID_MARQUE"})})
 * @ORM\Entity
 */
class CorrespondanceProduit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="REFERENCE", type="string", length=50, nullable=false)
     */
    private $reference;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_MARQUE", type="integer", nullable=false)
     */
    private $idMarque;

    /**
     * @var string
     *
     * @ORM\Column(name="NOM", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=50, nullable=false)
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="PRIX", type="float", precision=10, scale=0, nullable=false)
     */
    private $prix;

    /**
     * @var float
     *
     * @ORM\Column(name="TAUX_REDUCTION", type="float", precision=10, scale=0, nullable=false)
     */
    private $tauxReduction;

    /**
     * @var integer
     *
     * @ORM\Column(name="NOMBRE_POINT", type="integer", nullable=false)
     */
    private $nombrePoint;

    /**
     * @var string
     *
     * @ORM\Column(name="PHOTO", type="string", length=200, nullable=false)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="string", length=100, nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="QUANTITE", type="integer", nullable=false)
     */
    private $quantite;

    /**
     * @var integer
     *
     * @ORM\Column(name="QUANTITE_VENDU", type="integer", nullable=false)
     */
    private $quantiteVendu;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return CorrespondanceProduit
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set idMarque
     *
     * @param integer $idMarque
     *
     * @return CorrespondanceProduit
     */
    public function setIdMarque($idMarque)
    {
        $this->idMarque = $idMarque;

        return $this;
    }

    /**
     * Get idMarque
     *
     * @return integer
     */
    public function getIdMarque()
    {
        return $this->idMarque;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return CorrespondanceProduit
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return CorrespondanceProduit
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return CorrespondanceProduit
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set tauxReduction
     *
     * @param float $tauxReduction
     *
     * @return CorrespondanceProduit
     */
    public function setTauxReduction($tauxReduction)
    {
        $this->tauxReduction = $tauxReduction;

        return $this;
    }

    /**
     * Get tauxReduction
     *
     * @return float
     */
    public function getTauxReduction()
    {
        return $this->tauxReduction;
    }

    /**
     * Set nombrePoint
     *
     * @param integer $nombrePoint
     *
     * @return CorrespondanceProduit
     */
    public function setNombrePoint($nombrePoint)
    {
        $this->nombrePoint = $nombrePoint;

        return $this;
    }

    /**
     * Get nombrePoint
     *
     * @return integer
     */
    public function getNombrePoint()
    {
        return $this->nombrePoint;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return CorrespondanceProduit
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CorrespondanceProduit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return CorrespondanceProduit
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set quantiteVendu
     *
     * @param integer $quantiteVendu
     *
     * @return CorrespondanceProduit
     */
    public function setQuantiteVendu($quantiteVendu)
    {
        $this->quantiteVendu = $quantiteVendu;

        return $this;
    }

    /**
     * Get quantiteVendu
     *
     * @return integer
     */
    public function getQuantiteVendu()
    {
        return $this->quantiteVendu;
    }
}
