<?php

namespace Labess\BackofficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('LabessBackofficeBundle:Default:index.html.twig', array('name' => $name));
    }
}
