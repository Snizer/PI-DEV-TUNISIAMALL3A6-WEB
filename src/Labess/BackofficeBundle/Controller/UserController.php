<?php

namespace Labess\BackofficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use utilisateurs\utilisateursBundle\Entity\User;
class UserController extends Controller
{

    public function afficheResponsableAction()

    {
       $x = 'RESPONSABLE';
       $em=$this->getDoctrine()->getManager();
       $usr =$em->getRepository("utilisateursutilisateursBundle:User")->findByRole($x);
       
       return $this->render('LabessBackofficeBundle::AffichageResponsable.html.twig',array('PlusieurUser'=>$usr));
    }
    
    
    public function afficheClientAction()

    {
       $x = 'CLIENT';
       $em=$this->getDoctrine()->getManager();
       $usr =$em->getRepository("utilisateursutilisateursBundle:User")->findByRole($x);
       
       return $this->render('LabessBackofficeBundle::AffichageClient.html.twig',array('PlusieurUser'=>$usr));
    }
    
     
     
     
        public function deleteResponsableAction($id)
    {  
        $em=$this->getDoctrine()->getManager();
         
         $modele=$em->getRepository("utilisateursutilisateursBundle:User")->find($id);
     
         $em->remove($modele);
         $em->flush();
         
         // envoi mail
         $x='wissem.mahjoub@esprit.tn';
         $messag=  \Swift_Message::newInstance()
                 ->setSubject('Suppression de votre compte')
                 ->setFrom(array('mohamedali.boughanmi@esprit.tn' =>'Service Admin'))
                 ->setTo($modele->getEmailCanonical())
                 ->setCharset('utf-8')
                 ->setContentType('text/html')
                 ->setBody($this->render('LabessBackofficeBundle::Testt.html.twig',array('utilisateur' => $modele)));
         $this->get('mailer')->send($messag);
         
         
         
       return $this->redirect($this->generateUrl("labess_backoffice_afficheresponsable"));
       
       
    }
    
      public function deleteClientAction($id)
    {  
        $em=$this->getDoctrine()->getManager();
         
         $modele=$em->getRepository("utilisateursutilisateursBundle:User")->find($id);
     
         $em->remove($modele);
         $em->flush();
         
  
       return $this->redirect($this->generateUrl("labess_backoffice_afficheclient"));
       
       
    }
    
    
    
    public function blockrespAction($id)
    {  
        $query=$this->get('request');
         $em=$this->getDoctrine()->getManager();
         $modele=$em->getRepository('utilisateursutilisateursBundle:User')->find($id);         
    
        $query=$this->getDoctrine()->getManager()->createQuery('UPDATE utilisateursutilisateursBundle:User u SET  u.etat = 1 where u.id=:id');
        $query->setParameter('id',$id);
        $query->execute();
         
         // envoi mail
         
         $messag=  \Swift_Message::newInstance()
                ->setSubject('Bloquage de votre compte')
                 ->setFrom(array('mohamedali.boughanmi@esprit.tn' =>'Service Admin'))
                 ->setTo($modele->getEmail())
                 ->setCharset('utf-8')
                ->setContentType('text/html')
                 ->setBody($this->render('LabessBackofficeBundle::Block.html.twig',array('utilisateur' => $modele)));
         $this->get('mailer')->send($messag);
         
         
         
       return $this->redirect($this->generateUrl("labess_backoffice_afficheresponsable"));
       
       
    }
     public function deblockrespAction($id)
    {  
        $query=$this->get('request');
         $em=$this->getDoctrine()->getManager();
         $Membre=$em->getRepository('utilisateursutilisateursBundle:User')->find($id);         
    
        $query=$this->getDoctrine()->getManager()->createQuery('UPDATE utilisateursutilisateursBundle:User u SET  u.etat = 0 where u.id=:id');
        $query->setParameter('id',$id);
        $query->execute();
         

       return $this->redirect($this->generateUrl("labess_backoffice_afficheresponsable"));
       
       
    }
    
    
}
