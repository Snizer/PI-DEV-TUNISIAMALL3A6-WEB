<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MailController
 *
 * @author dali
 */
namespace Labess\BackofficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Labess\BackofficeBundle\Entity\Mail;
use Labess\BackofficeBundle\Form\MailType;
use Swift_Message;

class MailController extends Controller {
    
    
    public function indexAction() { 
        return $this->render('LabessBackofficeBundle::mail.html.twig', array()); 
        return 'Mail';
        }
     public function sendMailAction() { 
         $to = " "; 
         $mail =new Mail(); 
         $form= $this->createForm(new MailType(), $mail); 
         $request->get('request'); 
                 $form->handleRequest($request) ; 
         if ($form->isValid()) { 
             $message = Swift_Message::newInstance() 
                     ->setSubject($mail->getNom()) 
                     ->setFrom($mail-> getFrom()) 
                     ->setTo($to) 
                     ->setBody($mail->getText()); 
             $this->get('mailer')
                     ->send($message); 
             return $this->render('LabessBackofficeBundle::mail.html.twig', array('to' => $to, 'from' => $mail-> getFrom() )); 
             
         } 
         return $this->redirect($this->generateUrl('my_app_mail_form'));
         
         } 
         
         public function newAction() { 
             $mail = new Mail(); 
             $form= $this-> createForm(new MailType(), $mail); 
             $request = $this->get('request'); 
             $form->handleRequest($request) ; 
             if ($form->isValid()) { 
                 $this->sendMailAction('mohamedali.boughanmi@esprit.tn', 
                         $mail-> getFrom(), 
                         $mail->getNom(), 
                         $mail->getText()); 
                 
             } 
             return $this->render('LabessBackofficeBundle::new.html.twig', array('form' => $form->createView())) ; }
         
         }   
        

    
    
    

