<?php

namespace Labess\BackofficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Tunisiamall\tunisiamallBundle\Entity\Marque;
use utilisateurs\utilisateursBundle\Entity\User;
use Labess\BackofficeBundle\Form\MarqueForm;
class MarqueController extends Controller
{

    public function affichagemarqueAction()

    {
       $em=$this->getDoctrine()->getManager();
       $Marques =$em->getRepository("TunisiamalltunisiamallBundle:Marque")->findAll();
       
       return $this->render('LabessBackofficeBundle::AffcihageMarque.html.twig',array('PlusieurMarque'=>$Marques));
    }
     public function ajoutmarqueAction()
 {
//         $request=$this->get('request');
//        $mrq=new Marque();
//        if($request->getMethod()=='POST')
//        {
//         $mrq->setNom($request->get("nom"));
//         $mrq->setSeuilachat($request->get('seuil'));
//         $mrq->setIdResponsable($request->get('idresp'));
//         
//         $em=$this->getDoctrine()->getManager();
//         $em->persist($mrq);
//         $em->flush();
                     
//        }
//         return $this->render('LabessBackofficeBundle::Ajouter.html.twig', array()); 
          $Marque = new Marque;
          $x = 'RESPONSABLE';
       $em=$this->getDoctrine()->getManager();
       $usr =$em->getRepository("utilisateursutilisateursBundle:User")->findByRole($x);
       
          $form = $this->createFormBuilder($Marque)
                   ->add('nom')
                   ->add('seuilachat')
                   ->add('idResponsable','entity', array( 'class' => 'utilisateursutilisateursBundle:User', 'property'=>'nom'))
                   
                  ->add('Valider','submit')
                  ->getForm();
          $request = $this->getRequest();
          if($form->handleRequest($request)->isValid())
          {
              $em=  $this->getDoctrine()->getManager();
              $em->persist($Marque);
              $em->flush();
              return $this->redirect($this->generateUrl("labess_backoffice_affichemarque"));
          }
        return $this->render('LabessBackofficeBundle::Ajouter.html.twig',array('form'=> $form->createView()));
        
        

         
         


     }
     
     
        public function deletemarqueAction($id)
    {  
        $em=$this->getDoctrine()->getManager();
         
         $mrq=$em->getRepository("TunisiamalltunisiamallBundle:Marque")->find($id);
     
         $em->remove($mrq);
         $em->flush();
         
       return $this->redirect($this->generateUrl("labess_backoffice_affichemarque"));
       
       
    }
    
    
         public function updatemarqueAction($id){
        
        
       $em=$this->getDoctrine()->getManager();
       $marque=$em->getRepository("TunisiamalltunisiamallBundle:Marque")->find($id);
       $form = $this->createForm(new MarqueForm(),$marque);
       $request=$this->get('request');
       $form->handleRequest($request);
            if($form->isValid()){
                
                $em=$this->getDoctrine()->getManager();
                $em->persist($marque);
                $em->flush();
               return $this->redirect($this->generateUrl("labess_backoffice_affichemarque"));

            }
        
                     return $this->render('LabessBackofficeBundle::UpdateMarque.html.twig', array('form'=> $form->createView())); 

            
        
    }
    
}
