<?php

namespace Labess\BackofficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NavigationController extends Controller
{
    public function indexAction()
    {
        return $this->render('LabessBackofficeBundle::layout.html.twig');
    }
    
     public function ajoutAction()
    {
        return $this->render('LabessBackofficeBundle:Ajouter.html.twig');
    }
}

