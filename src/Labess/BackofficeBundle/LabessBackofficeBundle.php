<?php

namespace Labess\BackofficeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LabessBackofficeBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
