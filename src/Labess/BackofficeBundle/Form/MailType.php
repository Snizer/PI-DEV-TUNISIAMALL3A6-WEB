<?php


namespace Labess\BackofficeBundle\Form;



use Symfony\Component\Form\AbstractType; 
use Symfony\Component\Form\FormBuilderInterface; 
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MailType extends AbstractType {
    //put your code here
    public function buildForm(FormBuilderInterface $builder, array $options) { 
        $builder ->add('nom', 'text') 
                ->add('prenom', 'text') 
                ->add('tel', 'integer') 
                ->add('from', 'email') 
                ->add('text', 'textarea') 
                ->add('valider', 'submit') ; 
                
    } 
    public function getName() { 
        return 'Mail'; }
        
    }



