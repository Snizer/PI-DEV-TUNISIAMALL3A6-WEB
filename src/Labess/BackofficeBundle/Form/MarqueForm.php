<?php
namespace Labess\BackofficeBundle\Form;
use Symfony\Component\Form\AbstractType;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModeleForm
 *
 * @author Hatem
 */
class MarqueForm extends AbstractType {
    
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options) {
       $builder
               ->add('nom')
               ->add('seuilachat')
               ->setMethod('GET')
               ->add('Save','submit');
    }
    
    public function getName()
{
    return 'Marque';
}
}
