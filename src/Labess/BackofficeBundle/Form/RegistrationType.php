<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Labess\BackofficeBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegistrationType extends AbstractType
{
   

    public function getParent()
    {
        return 'fos_user_registration';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('nom',null,array('label'=>'Last Name','attr'=>array('class'=>'form-control')))
               ->add('prenom',null,array('label'=>'First Name','attr'=>array('class'=>'form-control')))
            ->add('adress',null,array('label'=>'adress','attr'=>array('class'=>'form-control')))
                ->add('email',LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('label' => 'Email', 'translation_domain' => 'FOSUserBundle','attr'=>array('class'=>'form-control')))
            ->add('username', null, array('label' => 'Username', 'translation_domain' => 'FOSUserBundle','attr'=>array('class'=>'form-control')))
            ->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
                'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'Password','attr'=>array('class'=>'form-control')),
                'second_options' => array('label' => 'Reeenter Password','attr'=>array('class'=>'form-control')),
               
                
            ))
                  ->add('sexe','choice',array('choices'=> array('Homme'=> 'Homme','Femme'=>'Femme'),'expanded'=>true))
        
            
//              ->add('roles', 'collection', array(
//                   'type' => 'hidden',
//                   'options' => array(
//                       'label' => false, /* Ajoutez cette ligne */
//                       'choices' => array(
//                          
//                           'ROLE_RESPONSABLE' => 'RESPONSABLE'
//                       )
//                   ),
//                 
//                  
//               )
//           )
              
                ->add('TELEPHONE',null,array('label'=>'TELEPHONE','attr'=>array('class'=>'form-control')))
                 ->add('CIN',null,array('label'=>'CIN','attr'=>array('class'=>'form-control')))
                ->add('QRCODE',null,array('label'=>'QRCODE','attr'=>array('class'=>'form-control')))
                 ->add('role','hidden',array('data'=>'RESPONSABLE'))
                ->add('roles','collection', array('data'=>array('ROLE_RESPONSABLE' => 'ROLE_RESPONSABLE')))
  
            ;
            
           // ;
    }

   
   
}
